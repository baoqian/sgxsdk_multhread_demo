#ifndef X86_PRANDOM_H
#define X86_PRANDOM_H

#include <memory>
#include <mutex>
#include <queue>
// #include <future>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <stdint.h>
#include <vector>

enum {
  ATF_PRANDOM_OK = 0,           ///< 操作正常完成
  ATF_PRANDOM_UNINIT_ERROR = 1, ///< 引擎未初始化
  ATF_PRANDOM_HW_ERROR = 2,     ///< 硬件错误
  ATF_PRANDOM_PARAM_ERROR = 3,  ///< 参数错误
  ATF_PRANDOM_SYS_ERROR = 4, ///< 系统错误，如内存、线程等资源不足
};

/**
 * @brief 初始化引擎的附加配置。
 */
typedef struct {
  uint32_t
      core; ///< 内部线程绑定的CPU核ID，如果初始化时不传配置，默认绑定到CPU0
  uint8_t log_enable; ///< 是否向stdout打印日志
} atf_prandom_cfg_t;

/**
 * @brief 获取库的版本号。
 *
 * @return  返回值的低24位为版号，按8位分隔，例如 0x010203表示版本v1.2.3
 */
int atf_prandom_get_version();

/**
 * @brief 初始化伪随机数引擎。
 *
 * @param seed 随机数种子，不可以为NULL
 * @param seed_len 随机数种子长度，不能为0
 * @param cfg 附加配置数据，可以为NULL
 * @return ATF_PRANDOM_OK或其它错误码
 */
int atf_prandom_init(const uint8_t *seed, uint32_t seed_len,
                     const atf_prandom_cfg_t *cfg);

/**
 * @brief 关闭伪随机数引擎，释放内部资源。
 * @return ATF_PRANDOM_OK或其它错误码
 */
int atf_prandom_deinit();

/* ----- 拷贝接口 ----- */
/**
 * @brief 生成随机数。
 *
 * @param buf 存放生成的随机数的内存地址
 * @param gen_len 想要生成的随机数长度
 * @return ATF_PRANDOM_OK或其它错误码
 */
int atf_prandom_gen(uint8_t *buf, uint32_t gen_len);

/* ----- 零拷贝接口 ----- */
/*
 * 零拷贝接口可直接获取内部的随机数buffer并使用，
 * 每次alloc一个block后，把所有数据用完，
 * 然后alloc下一个block，一直循环。
 * 注意：零拷贝接口和拷贝接口不要混合使用
 */

/**
 * @brief 获取内部block的大小。此值是固定值，只获取一次即可。
 * @return 内部block的大小
 */
uint32_t atf_prandom_get_block_size();

/**
 * @brief 获取一个内部的block buffer地址。
 *
 * 每次调用此函数获取的地址都不一样，将每次调用得到的block中数据按先后顺序排列，
 * 即为生成的伪随机数序列
 *
 * @return  内部的block buffer地址或 NULL（如果有内部错误）
 */
const uint8_t *atf_prandom_alloc_block();

extern int g_thread_count;  
// 设置线程数
void set_thread_count(int count);

#endif // ATF_PRANDOM_H