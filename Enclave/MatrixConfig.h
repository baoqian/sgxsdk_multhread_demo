/*
 * MatrixConfig.h
 *
 *  Created on: 2024年1月7日
 *      Author: xing
 */

#ifndef ENCLAVE_MATRIX_CONFIG_H_
#define ENCLAVE_MATRIX_CONFIG_H_

namespace matrix {
namespace {

typedef char A_TYPE;
typedef int32_t B_TYPE;
typedef int32_t C_TYPE;
typedef int32_t ProdType;

static constexpr int A_ROW = 11008;
static constexpr int A_COL = 4096;
static constexpr int B_ROW = A_COL;
static constexpr int C_ROW = A_ROW;

} /* namespace */
} /* namespace matrix */

#endif /* ENCLAVE_MATRIX_CONFIG_H_ */
