#ifndef ATF_PRANDOM_CPP
#define ATF_PRANDOM_CPP

#include "x86_prandom.h"
#include "Enclave.h"
#include <pthread.h>
#include <openssl/aes.h>
#include "sgx_thread.h"

int g_thread_count = 2;  

void set_thread_count(int count) {
    g_thread_count = count;
}

std::unique_ptr<std::vector<std::vector<unsigned char>>> datas = nullptr;
std::unique_ptr<std::vector<unsigned char>> key = nullptr;
std::unique_ptr<std::vector<std::vector<unsigned char>>> ivs = nullptr;
std::atomic<int> job(0);
bool isInitialized = false;
// EVP_CIPHER_CTX* ctx = nullptr;
EVP_MD_CTX *md_ctx = nullptr;
std::mutex mtx;
std::condition_variable cv;
std::queue<int> jobs;
// std::vector<std::future<void>> futures(g_thread_count);
std::vector<std::atomic<bool>> blocks_processed(64);

int atf_prandom_get_version() {
  return 0x010000; // assuming version is v1.0.0
}

typedef struct {
    sgx_thread_mutex_t mutex;
    sgx_thread_cond_t cond;
} cond_t;

static cond_t cond_t_var = {SGX_THREAD_MUTEX_INITIALIZER, SGX_THREAD_COND_INITIALIZER};

bool work = true;
std::vector<pthread_t> threads(g_thread_count);

void *worker_thread(void*) {
  EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new(); // thread-local context

  if (ctx == nullptr) {
    printf("Cannot create EVP_CIPHER_CTX\n");
    return nullptr;
  }

  while (work) {
    // std::unique_lock<std::mutex> lock(mtx);
    // cv.wait(lock, [] { return !jobs.empty(); });
    cond_t *t = &cond_t_var;
    sgx_thread_mutex_lock(&t->mutex);
    while (jobs.empty())
        sgx_thread_cond_wait(&t->cond, &t->mutex);

    int currentJob = jobs.front();
    jobs.pop();
    // lock.unlock();
    // sgx_thread_cond_signal(&t->cond);
    sgx_thread_mutex_unlock(&t->mutex);

    if (currentJob == -1) {
      EVP_CIPHER_CTX_free(ctx);
      return nullptr; // shutdown signal
    }

    if (EVP_EncryptInit_ex(ctx, EVP_sm4_cbc(), NULL, (unsigned char*)key->data(),
                           ivs->at(currentJob).data()) != 1) {
      printf("init error\n");
      EVP_CIPHER_CTX_free(ctx);
      return nullptr;
    }
    // if (EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), nullptr, key->data(),
    //                        ivs->at(currentJob).data()) != 1) {
    //   LOG("init error\n");
    //   EVP_CIPHER_CTX_free(ctx);
    //   return;
    // }
    int len;
    std::vector<unsigned char> output_data(16384);
    if (EVP_EncryptUpdate(ctx, output_data.data(), &len,
                          datas->at(currentJob).data(), 16384) != 1) {
      printf("encrypt error\n");
      EVP_CIPHER_CTX_free(ctx);
      return nullptr;
    }

    datas->at(currentJob) = output_data;
    std::memcpy(ivs->at(currentJob).data(), output_data.data() + len - 16, 16);

    blocks_processed[currentJob] = true;
  }
  pthread_exit(NULL);
}

int atf_prandom_init(const unsigned char *seed, uint32_t seed_len,
                     const atf_prandom_cfg_t *cfg) {
  datas = std::unique_ptr<std::vector<std::vector<unsigned char>>>(
      new std::vector<std::vector<unsigned char>>(64, std::vector<unsigned char>(16384)));
  key = std::unique_ptr<std::vector<unsigned char>>(new std::vector<unsigned char>(16));
  ivs = std::unique_ptr<std::vector<std::vector<unsigned char>>>(
      new std::vector<std::vector<unsigned char>>(64, std::vector<unsigned char>(16)));

//   OPENSSL_init_crypto(0, NULL);
  // Initialize SM3 hash context
  md_ctx = EVP_MD_CTX_new();
  if (!md_ctx) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  //   Initialize using SM3
  if (EVP_DigestInit_ex(md_ctx, EVP_sm3(), nullptr) != 1) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  //   Update hash context
  if (EVP_DigestUpdate(md_ctx, seed, seed_len) != 1) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  // Complete the hash operation, output to key_iv
  std::vector<unsigned char> key_iv(32);
  unsigned int key_iv_len;
  if (EVP_DigestFinal_ex(md_ctx, key_iv.data(), &key_iv_len) != 1) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  // Take the first 16 bits and the last 16 bits of key_iv as the key and
  // initial vector
  std::memcpy(key->data(), key_iv.data(), 16);
  for (int i = 0; i < 64; ++i) {
    std::memcpy(ivs->at(i).data(), key_iv.data() + 16, 16);
  }
  for (int i = 0; i < 64; i++) {
    std::fill(datas->at(i).begin(), datas->at(i).end(),
              static_cast<unsigned char>(i));
  }
//   for (int i = 0; i < g_thread_count; ++i) {
//     futures[i] = std::async(std::launch::async, worker_thread);
//   }
  for (int thread_id = 0; thread_id < g_thread_count; ++thread_id) {
    int status = pthread_create(&threads[thread_id], NULL, worker_thread, NULL);
    if (status != 0) {
        printf("status = %d, thread_id = %d\n", status, thread_id);
        return status;
    }
  }
  isInitialized = true;
  return ATF_PRANDOM_OK;
}

int atf_prandom_deinit() {
  // Signal worker threads to stop
  work = false;
  for (int thread_id = 0; thread_id < g_thread_count; ++thread_id) {
    pthread_join(threads[thread_id], NULL);
  }

  for (int i = 0; i < g_thread_count; ++i) {
    jobs.push(-1);
  }
//   cv.notify_all();
cond_t *t = &cond_t_var;
sgx_thread_cond_signal(&t->cond);

  // Wait for worker threads to finish
//   for (int i = 0; i < g_thread_count; ++i) {
//     futures[i].get();
//   }

  EVP_MD_CTX_free(md_ctx);
  datas.reset();
  key.reset();
  ivs.reset();

  isInitialized = false;
  return ATF_PRANDOM_OK;
}

uint32_t atf_prandom_get_block_size() {
  return 16384; // block size
}

const unsigned char *atf_prandom_alloc_block() {
  if (!isInitialized) {
    return nullptr;
  }

  int currentJob = job.fetch_add(1) % 64;
  if (currentJob == 0) {
    for (int i = 0; i < 64; ++i) {
      std::unique_lock<std::mutex> lock(mtx);
      jobs.push(i);
    }
    // cv.notify_all();
    cond_t *t = &cond_t_var;
sgx_thread_cond_signal(&t->cond);
  }
  while (!blocks_processed[currentJob])
    ;
  blocks_processed[currentJob] = false;
  return datas->at(currentJob).data();
}

#endif // ATF_PRANDOM_CPP