/*
 * util.h
 *
 *  Created on: 2024年1月4日
 *      Author: xing
 */

#ifndef SRC_INCLUDE_UTIL_H_
#define SRC_INCLUDE_UTIL_H_

namespace matrix {
namespace {

template<class T = int>
inline T CeilDiv(const T x, const T y) {
	return (x + y - 1) / y;
}

template<class AType, class BType, class ProdType>
inline ProdType Prod(const AType a, const BType b) {
	return a * (ProdType) b;
}

template<>
inline signed int Prod<signed char, signed short, signed int>(
		const signed char a, const signed short b) {
	return a * (signed int) b;
}

} /* namespace */
} /* namespace matrix */

#endif /* SRC_INCLUDE_UTIL_H_ */
