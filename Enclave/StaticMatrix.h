/*
 * StaticMatrix.h
 *
 *  Created on: 2023年12月30日
 *	  Author: xing
 */

#ifndef INCLUDE_STATICMATRIX_H_
#define INCLUDE_STATICMATRIX_H_

#include "util.h"
#include <pthread.h>
#include <vector>

namespace matrix {

template<int _NRows, int _NCols, class _DataType>
class StaticMatrixBase {
public:
	typedef _DataType DataType;
	static const int NRows = _NRows;
	static const int NCols = _NCols;

	DataType *data() { return data_; }
	const DataType *data() const { return data_; }

protected:
	DataType data_[NRows * NCols];
};

template<int _NRows, int _NCols, class _DataType>
class StaticMatrixRowMajor : public StaticMatrixBase<_NRows, _NCols, _DataType> {
public:
	static const int NRows = _NRows;
	static const int NCols = _NCols;
	typedef _DataType DataType;
	typedef StaticMatrixBase<NRows, NCols, DataType> BaseType;

	DataType& At(int row, int column) {
		return BaseType::data_[row * NCols + column];
	}

	const DataType& At(int row, int column) const {
		return BaseType::data_[row * NCols + column];
	}
};

template<int _NRows, int _NCols, class _DataType>
class StaticMatrixColumnMajor : public StaticMatrixBase<_NRows, _NCols, _DataType> {
public:
	static const int NRows = _NRows;
	static const int NCols = _NCols;
	typedef _DataType DataType;
	typedef StaticMatrixBase<NRows, NCols, DataType> BaseType;

	DataType& At(int row, int column) {
		return BaseType::data_[column * NRows + column];
	}

	const DataType& At(int row, int column) const {
		return BaseType::data_[column * NRows + column];
	}
};

template<int NRows, int NCols, class DataType, bool RowMajor = true>
class StaticMatrix {
	// Empty
};

template<int _NRows, class _DataType, bool _RowMajor>
class StaticColumnVector;

template<int _NRows, int _NCols, class _DataType>
class StaticMatrix<_NRows, _NCols, _DataType, true> : public StaticMatrixRowMajor<_NRows, _NCols, _DataType> {
public:
	static constexpr bool RowMajor = true;
	static const int NRows = _NRows;
	static const int NCols = _NCols;

	typedef _DataType DataType;
	typedef StaticMatrixRowMajor<_NRows, _NCols, _DataType> BaseType;

	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, int _OtherNCols, class _OtherDataType, bool _OtherRowMajor,
			int _ResNRows, int _ResNCols, class _ResDataType, bool _ResRowMajor>
	void MatMul(const StaticMatrix<_OtherNRows, _OtherNCols, _OtherDataType, _OtherRowMajor> &other,
			StaticMatrix<_ResNRows, _ResNCols, _ResDataType, _ResRowMajor> *result) const {}

	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, class _OtherDataType,
			int _ResNRows, class _ResDataType>
	void MatMul(const StaticMatrix<_OtherNRows, 1, _OtherDataType, true> &other,
			StaticMatrix<_ResNRows, 1, _ResDataType, true> *result, int star_row, int end_row) const {
		const int chunk_size = CeilDiv(NCols, _MatrixBSplit);
		for (int a_row = star_row; a_row < end_row && a_row < NRows; ++a_row) {
			_ProdType sum = 0;
			for (int i = 0; i < _MatrixBSplit; ++i) {
				const int chunk_offset = chunk_size * i;
				const int chunks = (NCols - chunk_offset > chunk_size) ? chunk_size : (NCols - chunk_offset);
				for (int j = chunk_offset; j < chunk_offset + chunks && j < NCols; ++j) {
					sum += Prod<_DataType, _OtherDataType, _ProdType>(this->At(a_row, j), other.At(j, 0));
				}
			}
			result->At(a_row, 0) = sum;
		}
	}

	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, class _OtherDataType,
			int _ResNRows, class _ResDataType>
	void MatVecMul(const StaticColumnVector<_OtherNRows, _OtherDataType, true> &vec,
			StaticColumnVector<_ResNRows, _ResDataType, true> *result, int num_thread = 1, int START_ROW = 0, int END_ROW = NRows) const {
		if (num_thread > 1) {
			MatVecMulMultiThread<_ProdType, _MatrixBSplit, _OtherNRows, _OtherDataType, _ResNRows, _ResDataType>(vec, result, num_thread);
			return;
		}
		MatMul<_ProdType, _MatrixBSplit, _OtherNRows, _OtherDataType, _ResNRows, _ResDataType>(vec, result, START_ROW, END_ROW);
	}

	struct data {
		const StaticMatrix<_NRows, _NCols, _DataType, true> *a;
		const StaticMatrix<4096, 1, int32_t, true> *other;
		StaticMatrix<11008, 1, int32_t, true> *result;
		int num_thread;
		int thread_id;
	};

    // static void* threadFunction(void* instance) {
    //     // 将参数转换为类实例
    //     StaticMatrix<_NRows, _NCols, _DataType, true>* myInstance = static_cast<StaticMatrix<_NRows, _NCols, _DataType, true>*>(instance);
    //     myInstance->func();  // 调用方法B

    //     pthread_exit(nullptr);
    // }

	static void *func(void *arg) {
		data *data_ptr = (data*)(arg);
		const StaticMatrix<_NRows, _NCols, _DataType, true> *a = data_ptr->a;
		const StaticMatrix<4096, 1, int32_t, true> &other = *(data_ptr->other);
		StaticMatrix<11008, 1, int32_t, true> *result = data_ptr->result;
		int num_thread = data_ptr->num_thread;;
		int thread_id = data_ptr->thread_id;;

		const int row_chunk_size = CeilDiv(NRows, num_thread);
		const int start_row = thread_id * row_chunk_size;
		const int row_size = (NRows - start_row > row_chunk_size) ? row_chunk_size : (NRows - start_row);
		const int end_row = start_row + row_size;
		const int chunk_size = CeilDiv(NCols, 4);
		printf("func pthread %d row(%d %d)\n", thread_id, start_row, end_row);
		for (int a_row = start_row; a_row < end_row && a_row < NRows; ++a_row) {
			int32_t sum = 0;
			for (int i = 0; i < 4; ++i) {
				const int chunk_offset = chunk_size * i;
				const int chunks = (NCols - chunk_offset > chunk_size) ? chunk_size : (NCols - chunk_offset);
				for (int j = chunk_offset; j < chunk_offset + chunks && j < NCols; ++j) {
					sum += Prod<char, int32_t, int32_t>(a->At(a_row, j), other.At(j, 0));
				}
			}
			result->At(a_row, 0) = sum;
		}
		pthread_exit(NULL);
	}
	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, class _OtherDataType,
			int _ResNRows, class _ResDataType>
	void MatVecMulMultiThread(const StaticMatrix<_OtherNRows, 1, _OtherDataType, true> &other,
			StaticMatrix<_ResNRows, 1, _ResDataType, true> *result, int num_thread = 1) const {
		// std::vector<std::thread *> threads(num_thread);
		// int matrix_b_split = _MatrixBSplit;
		// for (int thread_id = 0; thread_id < num_thread; ++thread_id) {
		// 	threads[thread_id] = new std::thread([this, &other, result, num_thread, thread_id, matrix_b_split]() {
		// 		const int row_chunk_size = CeilDiv(NRows, num_thread);
		// 		const int start_row = thread_id * row_chunk_size;
		// 		const int row_size = (NRows - start_row > row_chunk_size) ? row_chunk_size : (NRows - start_row);
		// 		const int end_row = start_row + row_size;

		// 		const int chunk_size = CeilDiv(NCols, _MatrixBSplit);
		// 		for (int a_row = start_row; a_row < end_row && a_row < NRows; ++a_row) {
		// 			_ProdType sum = 0;
		// 			for (int i = 0; i < _MatrixBSplit; ++i) {
		// 				const int chunk_offset = chunk_size * i;
		// 				const int chunks = (NCols - chunk_offset > chunk_size) ? chunk_size : (NCols - chunk_offset);
		// 				for (int j = chunk_offset; j < chunk_offset + chunks && j < NCols; ++j) {
		// 					sum += Prod<_DataType, _OtherDataType, _ProdType>(this->At(a_row, j), other.At(j, 0));
		// 				}
		// 			}
		// 			result->At(a_row, 0) = sum;
		// 		}
		// 	});
		// }

		// for (int thread_id = 0; thread_id < num_thread; ++thread_id) {
		// 	if (threads[thread_id]->joinable()) {
		// 		threads[thread_id]->join();
		// 	}
		// }

		// for (int thread_id = 0; thread_id < num_thread; ++thread_id) {
		// 	delete threads[thread_id];
		// }
		std::vector<pthread_t> threads(num_thread);
		using thred_function = void* (*)(void*);
		for (int thread_id = 0; thread_id < num_thread; ++thread_id) {
			// auto matmul = [this, &other, result, num_thread, thread_id](void *) -> void* {
			// 	const int row_chunk_size = CeilDiv(NRows, num_thread);
			// 	const int start_row = thread_id * row_chunk_size;
			// 	const int row_size = (NRows - start_row > row_chunk_size) ? row_chunk_size : (NRows - start_row);
			// 	const int end_row = start_row + row_size;
			// 	const int chunk_size = CeilDiv(NCols, _MatrixBSplit);
			// 	printf("pthread %d row(%d %d)\n", thread_id, start_row, end_row);
			// 	for (int a_row = start_row; a_row < end_row && a_row < NRows; ++a_row) {
			// 		_ProdType sum = 0;
			// 		for (int i = 0; i < _MatrixBSplit; ++i) {
			// 			const int chunk_offset = chunk_size * i;
			// 			const int chunks = (NCols - chunk_offset > chunk_size) ? chunk_size : (NCols - chunk_offset);
			// 			for (int j = chunk_offset; j < chunk_offset + chunks && j < NCols; ++j) {
			// 				sum += Prod<_DataType, _OtherDataType, _ProdType>(this->At(a_row, j), other.At(j, 0));
			// 			}
			// 		}
			// 		result->At(a_row, 0) = sum;
			// 	}
			// 	pthread_exit(NULL);
			// 	return nullptr;
			// };

			// thred_function lambda_as_function = [](void *arg) -> void* {
			// 	auto *matmul_func = reinterpret_cast<decltype(matmul)*>(arg);
			// 	(*matmul_func)(nullptr);
			// 	return nullptr;
			// };

			// int status = pthread_create(&threads[thread_id], NULL, lambda_as_function, &matmul);

			data d;
			d.a = this;
			d.other = &other;
			d.result = result;
			d.num_thread = num_thread;
			d.thread_id = thread_id;
			int status = pthread_create(&threads[thread_id], NULL, &StaticMatrix<_NRows, _NCols, _DataType, true>::func, (void *)&d);
			if (status != 0) {
				return;
			}
		}
		for (int thread_id = 0; thread_id < num_thread; ++thread_id) {
			pthread_join(threads[thread_id], NULL);
		}

	}
};

template<int _NRows, int _NCols, class _DataType>
class StaticMatrix<_NRows, _NCols, _DataType, false> : public StaticMatrixColumnMajor<_NRows, _NCols, _DataType> {
public:
	static constexpr bool RowMajor = false;
	static const int NRows = _NRows;
	static const int NCols = _NCols;

	typedef _DataType DataType;
	typedef StaticMatrixColumnMajor<_NRows, _NCols, _DataType> BaseType;
};

template<int _NRows, class _DataType, bool _RowMajor = true>
class StaticColumnVector : public StaticMatrix<_NRows, 1, _DataType, _RowMajor> {
public:
	static constexpr bool RowMajor = _RowMajor;
	static const int NRows = _NRows;

	typedef _DataType DataType;
	typedef StaticMatrix<NRows, 1, DataType, RowMajor> BaseType;

	DataType &operator[](int idx) {
		return BaseType::At(idx, 0);
	}

	const DataType &operator[](int idx) const {
		return BaseType::At(idx, 0);
	}
};

template<int _NCols, class _DataType, bool _RowMajor = true>
class StaticRowVector : public StaticMatrix<1, _NCols, _DataType, _RowMajor> {
public:
	static constexpr bool RowMajor = _RowMajor;
	static const int NCols = _NCols;

	typedef _DataType DataType;
	typedef StaticMatrix<1, NCols, DataType, RowMajor> BaseType;

	DataType &operator[](int idx) {
		return BaseType::At(0, idx);
	}

	const DataType &operator[](int idx) const {
		return BaseType::At(0, idx);
	}
};

} /* namespace matrix */

#endif /* INCLUDE_STATICMATRIX_H_ */
