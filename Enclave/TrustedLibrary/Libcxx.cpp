#include "../Enclave.h"
#include "Enclave_t.h"
#include "../StaticMatrix.h"
#include "../MatrixConfig.h"
#include "sgx_thread.h"
#include <pthread.h>
#include <memory>

#include "../Enclave.h"
#include "../x86_prandom.h"

int CeilDiv(const int x, const int y) {
	return (x + y - 1) / y;
}

void matmul1(int32_t *a, char *b, int32_t *c, const int chunk_size, int star_row, int matrix_a_split, int end_row, int k = 4096, int n = 11008) {
	for (int b_row = star_row; b_row < end_row && b_row < n; ++b_row) {
		register int ccc = 0;
		for (int i = 0; i < matrix_a_split; ++i) {
			const int chunk_offset = chunk_size * i;
			const int chunks = (k - chunk_offset > chunk_size) ? chunk_size : (k - chunk_offset);
			for (int j = chunk_offset; j < chunk_offset + chunks && j < k; ++j) {
				ccc += a[j] * b[b_row * k + j];
			}
		}
		c[b_row] += ccc;
	}
}

int32_t *aa = nullptr;
int32_t *cc = nullptr;
void ecall_init_data(int m, int k, int n) {
	aa = new int32_t[m * k];
	for (int i = 0; i < m; ++i) {
		for (int j = 0; j < k; ++j) {
			aa[i * k + j] = i + j;
		}
	}
	cc = new int32_t[m * n];

	for (int i = 0; i < m; ++i) {
		for (int j = 0; j < n; ++j) {
			cc[i * n + j] = 0;
		}
	}
}

void ecall_check_data(int m, int k, int n, char *b) {
	int32_t *c = new int32_t[m * n];
	for (int i = 0; i < m; ++i) {
		for (int j = 0; j < n; ++j) {
			c[i * n + j] = 0;
		}
	}

	for (int i = 0; i < m; ++i) {
		for (int j = 0; j < n; ++j) {
			for (int kk = 0; kk < k; ++kk) {
				c[i * n + j] += aa[i * k + kk] * b[kk * n + j];
			}
		}
	}

	for (int i = 0; i < m; ++i) {
		for (int j = 0 ; j < n; ++j) {
			if (cc[i * n + j] != c[i * n + j]) {
				printf("error cc[%d] = %d, c[%d] = %d\n", i * n + j, cc[i * n + j], i * n + j, c[i * n + j]);
				break;
			}
		}
	}

	delete []c;
	delete []aa;
	delete []cc;
}


void ecall_mul_pthread(void *static_matrix, int num_thread) {
	printf("ecall_mul_pthread\n");
	matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE> *a  = (matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE> *)static_matrix;

	std::unique_ptr<matrix::StaticColumnVector<matrix::B_ROW, matrix::B_TYPE>> b(new matrix::StaticColumnVector<matrix::B_ROW, matrix::B_TYPE>());
	for (int i = 0; i < matrix::B_ROW; ++i) {
		(*b)[i] = i;
	}

	std::unique_ptr<matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>> c(new matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>());
	for (int i = 0; i < matrix::C_ROW; ++i) {
		(*c)[i] = 0;
	}
	std::unique_ptr<matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>> check_res(new matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>());
	for (int i = 0; i < matrix::C_ROW; ++i) {
		(*check_res)[i] = 0;
	}

	for (int i = 0; i < matrix::A_ROW; ++i) {
		for (int j = 0; j < matrix::A_COL; ++j) {
			(*check_res)[i] += a->At(i, j) * (*b)[j];
		}
	}

	printf("num_thread = %d, %dx%d * %dx%d\n", num_thread, matrix::A_ROW, matrix::A_COL, matrix::B_ROW, 1);
	get_time_start();
	a->MatVecMul<matrix::ProdType, 4, matrix::B_ROW, matrix::B_TYPE, matrix::C_ROW, matrix::C_TYPE>(*b, c.get(), num_thread);
	get_time_end();

	for (int i = 0; i < matrix::C_ROW; ++i) {
		if ((*c)[i] != (*check_res)[i]) {
			printf("error @%d expect %d vs %d\n", i, (*check_res)[i], (*c)[i]);
			break;
		}
	}
}

std::unique_ptr<matrix::StaticColumnVector<matrix::B_ROW, matrix::B_TYPE>> static_matrix_b(new matrix::StaticColumnVector<matrix::B_ROW, matrix::B_TYPE>());
std::unique_ptr<matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>> static_matrix_c(new matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>());
std::unique_ptr<matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>> static_matrix_check_res(new matrix::StaticColumnVector<matrix::C_ROW, matrix::C_TYPE>());
void ecall_init(void *static_matrix) {
	matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE> *a  = (matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE> *)static_matrix;
	for (int i = 0; i < matrix::B_ROW; ++i) {
		(*static_matrix_b)[i] = i;
	}
	for (int i = 0; i < matrix::C_ROW; ++i) {
		(*static_matrix_c)[i] = 0;
	}
	for (int i = 0; i < matrix::C_ROW; ++i) {
		(*static_matrix_check_res)[i] = 0;
	}

	for (int i = 0; i < matrix::A_ROW; ++i) {
		for (int j = 0; j < matrix::A_COL; ++j) {
			(*static_matrix_check_res)[i] += a->At(i, j) * (*static_matrix_b)[j];
		}
	}
}

void ecall_check() {
	for (int i = 0; i < matrix::C_ROW; ++i) {
		if ((*static_matrix_c)[i] != (*static_matrix_check_res)[i]) {
			printf("error @%d expect %d vs %d\n", i, (*static_matrix_check_res)[i], (*static_matrix_c)[i]);
			break;
		}
	}
	printf("ecall_mul_stdthread check success\n\n");
}

void ecall_mul_stdthread(void *static_matrix, int num_thread, int thread_id) {
	matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE> *a  = (matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE> *)static_matrix;
	const int row_chunk_size = CeilDiv(matrix::A_ROW, num_thread);
	const int start_row = thread_id * row_chunk_size;
	const int row_size = (matrix::A_ROW - start_row > row_chunk_size) ? row_chunk_size : (matrix::A_ROW - start_row);
	const int end_row = start_row + row_size;
	get_time_start();
	a->MatVecMul<matrix::ProdType, 4, matrix::B_ROW, matrix::B_TYPE, matrix::C_ROW, matrix::C_TYPE>(*static_matrix_b, static_matrix_c.get(), 1, start_row, end_row);
	get_time_end();
}

void ecall_mul_thread(char *b, char *b_transposed, int m, int k, int n, int matrix_a_split, int num_thread, int thread_id)
{
    (void)b;
	(void)m;
	const int matrix_a_chunk_size = CeilDiv(k, matrix_a_split);
	const int row_chunk_size = CeilDiv(n, num_thread);
	const int start_row = thread_id * row_chunk_size;
	const int row_size = (n - start_row > row_chunk_size) ? row_chunk_size : (n - start_row);
	const int end_row = start_row + row_size;

	get_time_start();
	matmul1(aa, b_transposed, cc, matrix_a_chunk_size, start_row, matrix_a_split, end_row, k, n); // a(1x4096) b_transposed(11008x4096) c(1x11008)
	get_time_end();
}

size_t global_res = 0;
static sgx_thread_mutex_t global_mutex = SGX_THREAD_MUTEX_INITIALIZER;

size_t ecall_add(int thread_id) {
	printf("thread_id = %d\n", thread_id);
	size_t res = 0;
    for (int i = 0; i < 100000; i++) {
        sgx_thread_mutex_lock(&global_mutex);
		global_res++;
		// if (global_res == 100000 || global_res == 200000 || global_res == 300000 || global_res == 400000) {
		// 	printf("global_res = %d\n", global_res);
		// }
		// if (global_res = 4 * 100000) {
		// 	size_t res = global_res;
		// }
		// printf("global_res = %d\n", global_res);
        sgx_thread_mutex_unlock(&global_mutex);
    }
	printf("global_res = %d\n", global_res);
    return global_res;
}

static volatile uint64_t g_total = 0;
static volatile uint64_t g_result = 0;

void *gen_speed(void *arg) {
  uint32_t block_size = atf_prandom_get_block_size();
  uint32_t u64_count = block_size / 8;
  while (1) {
#if 1
    const uint64_t *buffer =
        reinterpret_cast<const uint64_t *>(atf_prandom_alloc_block());
#else
    uint64_t buffer[16384 / 8] = {0};
    atf_prandom_gen(reinterpret_cast<uint8_t *>(buffer), block_size);
#endif
    uint64_t result = 0;
    for (uint32_t i = 0; i < u64_count; i++) {
      // 1/8的数据做乘法，7/8的数据做加法
      if ((i & 0x07) == 0) {
        result *= buffer[i];
      } else {
        result += buffer[i];
      }
    }
    g_result = result;
    g_total += block_size;
  }
  return nullptr;
}

// void sig_int_proc(int sig) {
//   if (atf_prandom_deinit() != ATF_PRANDOM_OK) {
//     printf("atf_prandom_deinit failed\n");
//   }
//   return;
// }

/* SM4 block cipher mode(cbc) of operation. */
int ecall_sm4_cbc(void)
{	
	printf("evp_smx.c ecall_sm4_cbc\n");
	// Plain text
	unsigned char plainText[16] = { 0xAA,0xAA,0xAA,0xAA,0xBB,0xBB,0xBB,0xBB,0xCC,0xCC,0xCC,0xCC,0xDD,0xDD,0xDD,0xDD };
	// Secret key
	unsigned char key[16] = { 0x01,0x23,0x45,0x67,0x89,0xAB,0xCD,0xEF,0xFE,0xDC,0xBA,0x98,0x76,0x54,0x32,0x10 };
	// Initialization vector for CBC mode
	unsigned char iv[16] = { 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F };

	unsigned char encryptedText[16] = {};
	unsigned char decryptedText[16] = {};

	int len = 0;
	EVP_CIPHER_CTX* evp_ctx = NULL;
	int pad = 0;
	int ret = 0;

	// 1. Create and initialize ctx
	if (!(evp_ctx = EVP_CIPHER_CTX_new())) {
		printf("Error: fail to initialize EVP_CIPHER_CTX\n");
		ret = -1;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}

	// 2. Initialize encrypt, key and iv
	printf("11111\n");
	int r = EVP_EncryptInit_ex(evp_ctx, EVP_sm4_cbc(), NULL, (unsigned char*)key, iv);
	printf("22222\n");
	if (r != 1) {
		printf("Error: fail to initialize encrypt, key and iv\n");
		ret = -2;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}
	printf("r = %d\n", r);
	if (EVP_CIPHER_CTX_set_padding(evp_ctx, pad) != 1) {
		printf("Error: fail to set padding\n");
		ret = -3;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}

	// 3. Encrypt the plaintext and obtain the encrypted output
	if (EVP_EncryptUpdate(evp_ctx, encryptedText, &len, plainText, sizeof(plainText)) != 1) {
		printf("Error: fail to encrypt the plaintext\n");
		ret = -4;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}

	// 4. Finalize the encryption
	if (EVP_EncryptFinal_ex(evp_ctx, encryptedText + len, &len) != 1) {
		printf("Error: fail to finalize the encryption\n");
		ret = -5;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}

	// 5. Initialize decrypt, key and IV
	if (!EVP_DecryptInit_ex(evp_ctx, EVP_sm4_cbc(), NULL, (unsigned char*)key, iv)) {
		printf("Error: fail to initialize decrypt, key and IV\n");
		ret = -6;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}

	// 6. Decrypt the ciphertext and obtain the decrypted output
	if (!EVP_DecryptUpdate(evp_ctx, decryptedText, &len, encryptedText, sizeof(encryptedText))) {
		printf("Error: fail to decrypt the ciphertext\n");
		ret = -7;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}

	// 7. Finalize the decryption:
	// If length of decrypted data is integral multiple of 16, do not execute EVP_DecryptFinal_ex(), or it will fail to decrypt
	// - A positive return value indicates success;
	// - Anything else is a failure - the plaintext is not trustworthy.
	if (sizeof(decryptedText) % 16 != 0) {
		if (EVP_DecryptFinal_ex(evp_ctx, decryptedText + len, &len) <= 0) {
			printf("Error: fail to finalize the decryption\n");
			ret = -8;
			EVP_CIPHER_CTX_free(evp_ctx);
			return ret;
		}
	}

	// 8. Compare original and decrypted text
	if (memcmp(plainText, decryptedText, sizeof(plainText)) != 0) {
		printf("Error: original and decrypted text is different\n");
		ret = -9;
		EVP_CIPHER_CTX_free(evp_ctx);
		return ret;
	}
	// 9. Clean up and return
	EVP_CIPHER_CTX_free(evp_ctx);
	printf("ecall_sm4_cbc success\n");
	return ret;
}

void ecall_test_prandom() {
  atf_prandom_cfg_t cfg = {.core = 0};
  uint8_t seed[8] = {1, 2, 3, 4, 5, 6, 7, 8};
  int version = atf_prandom_get_version();
  printf("atf_prandom version: v%d.%d.%d\n", (version >> 16) & 0xff,
         (version >> 8) & 0xff, version & 0xff);

  if (atf_prandom_init(seed, 8, &cfg) != ATF_PRANDOM_OK) {
    printf("atf_prandom_init failed\n");
    return;
  }
  printf("atf_prandom_init success\n");
  // signal(SIGINT, sig_int_proc);

  pthread_t th;
  pthread_create(&th, nullptr, gen_speed, nullptr);
  // pthread_setname_np(th, "demo/gen_speed");
  uint64_t old_total = 0;
  while (1) {
    usleep(); //usleep(1000000);
    uint64_t cur_total = g_total;
    printf("speed: %.1f Gbps\n", (cur_total - old_total) * 8 / 1000000000.0);
    old_total = cur_total;
  }

//   ecall_sm4_cbc();
  return;
}

#define BUFFER_SIZE 50

typedef struct {
    int buf[BUFFER_SIZE];
    int occupied;
    int nextin;
    int nextout;
    sgx_thread_mutex_t mutex;
    sgx_thread_cond_t more;
    sgx_thread_cond_t less;
} cond_buffer_t;

static cond_buffer_t buffer = {{0, 0, 0, 0, 0, 0}, 0, 0, 0,
    SGX_THREAD_MUTEX_INITIALIZER, SGX_THREAD_COND_INITIALIZER, SGX_THREAD_COND_INITIALIZER};

void ecall_producer(void)
{
    for (int i = 0; i < 4*10; i++) {
        cond_buffer_t *b = &buffer;
        sgx_thread_mutex_lock(&b->mutex);
        while (b->occupied >= BUFFER_SIZE)
            sgx_thread_cond_wait(&b->less, &b->mutex);
        b->buf[b->nextin] = b->nextin;
        b->nextin++;
        b->nextin %= BUFFER_SIZE;
        b->occupied++;
        sgx_thread_cond_signal(&b->more);
        sgx_thread_mutex_unlock(&b->mutex);
    }
}

void ecall_consumer(void)
{
    for (int i = 0; i < 10; i++) {
        cond_buffer_t *b = &buffer;
        sgx_thread_mutex_lock(&b->mutex);
        while(b->occupied <= 0)
            sgx_thread_cond_wait(&b->more, &b->mutex);
        printf("%d = %d\n", b->nextout, b->buf[b->nextout]);
        b->buf[b->nextout++] = 0;
        b->nextout %= BUFFER_SIZE;
        b->occupied--;
        sgx_thread_cond_signal(&b->less);
        sgx_thread_mutex_unlock(&b->mutex);
    }
}