#ifndef ATF_PRANDOM_CPP
#define ATF_PRANDOM_CPP

#include "app_x86_prandom.h"
#include <openssl/aes.h>

int g_thread_count = 32;  

void set_thread_count(int count) {
    g_thread_count = count;
}

std::unique_ptr<std::vector<std::vector<uint8_t>>> datas = nullptr;
std::unique_ptr<std::vector<uint8_t>> key = nullptr;
std::unique_ptr<std::vector<std::vector<uint8_t>>> ivs = nullptr;
std::atomic<int> job(0);
bool isInitialized = false;
// EVP_CIPHER_CTX* ctx = nullptr;
EVP_MD_CTX *md_ctx = nullptr;
std::mutex mtx;
std::condition_variable cv;
std::queue<int> jobs;
std::vector<std::future<void>> futures(g_thread_count);
std::vector<std::atomic<bool>> blocks_processed(64);

int atf_prandom_get_version() {
  return 0x010000; // assuming version is v1.0.0
}

void worker_thread() {
  EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new(); // thread-local context

  if (ctx == nullptr) {
    printf("Cannot create EVP_CIPHER_CTX\n");
    return;
  }

  while (true) {
    std::unique_lock<std::mutex> lock(mtx);
    cv.wait(lock, [] { return !jobs.empty(); });

    int currentJob = jobs.front();
    jobs.pop();
    lock.unlock();
    if (currentJob == -1) {
      EVP_CIPHER_CTX_free(ctx);
      return; // shutdown signal
    }
    if (EVP_EncryptInit_ex(ctx, EVP_sm4_cbc(), nullptr, key->data(),
                           ivs->at(currentJob).data()) != 1) {
      printf("init error\n");
      EVP_CIPHER_CTX_free(ctx);
      return;
    }
    // if (EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), nullptr, key->data(),
    //                        ivs->at(currentJob).data()) != 1) {
    //   LOG("init error\n");
    //   EVP_CIPHER_CTX_free(ctx);
    //   return;
    // }
    int len;
    std::vector<uint8_t> output_data(16384);
    if (EVP_EncryptUpdate(ctx, output_data.data(), &len,
                          datas->at(currentJob).data(), 16384) != 1) {
      printf("encrypt error\n");
      EVP_CIPHER_CTX_free(ctx);
      return;
    }
    datas->at(currentJob) = output_data;
    std::memcpy(ivs->at(currentJob).data(), output_data.data() + len - 16, 16);

    blocks_processed[currentJob] = true;
  }
}

int atf_prandom_init(const uint8_t *seed, uint32_t seed_len,
                     const atf_prandom_cfg_t *cfg) {
  datas = std::unique_ptr<std::vector<std::vector<uint8_t>>>(
      new std::vector<std::vector<uint8_t>>(64, std::vector<uint8_t>(16384)));
  key = std::unique_ptr<std::vector<uint8_t>>(new std::vector<uint8_t>(16));
  ivs = std::unique_ptr<std::vector<std::vector<uint8_t>>>(
      new std::vector<std::vector<uint8_t>>(64, std::vector<uint8_t>(16)));
  // Initialize SM3 hash context
  md_ctx = EVP_MD_CTX_new();
  if (!md_ctx) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  // Initialize using SM3
  if (EVP_DigestInit_ex(md_ctx, EVP_sm3(), nullptr) != 1) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  // Update hash context
  if (EVP_DigestUpdate(md_ctx, seed, seed_len) != 1) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  // Complete the hash operation, output to key_iv
  std::vector<uint8_t> key_iv(32);
  unsigned int key_iv_len;
  if (EVP_DigestFinal_ex(md_ctx, key_iv.data(), &key_iv_len) != 1) {
    return ATF_PRANDOM_SYS_ERROR;
  }
  // Take the first 16 bits and the last 16 bits of key_iv as the key and
  // initial vector
  std::memcpy(key->data(), key_iv.data(), 16);
  for (int i = 0; i < 64; ++i) {
    std::memcpy(ivs->at(i).data(), key_iv.data() + 16, 16);
  }
  for (int i = 0; i < 64; i++) {
    std::fill(datas->at(i).begin(), datas->at(i).end(),
              static_cast<uint8_t>(i));
  }
  for (int i = 0; i < g_thread_count; ++i) {
    futures[i] = std::async(std::launch::async, worker_thread);
  }

  isInitialized = true;
  return ATF_PRANDOM_OK;
}

int atf_prandom_deinit() {
  // Signal worker threads to stop
  for (int i = 0; i < g_thread_count; ++i) {
    jobs.push(-1);
  }
  cv.notify_all();

  // Wait for worker threads to finish
  for (int i = 0; i < g_thread_count; ++i) {
    futures[i].get();
  }

  EVP_MD_CTX_free(md_ctx);
  datas.reset();
  key.reset();
  ivs.reset();

  isInitialized = false;
  return ATF_PRANDOM_OK;
}

uint32_t atf_prandom_get_block_size() {
  return 16384; // block size
}

const uint8_t *atf_prandom_alloc_block() {
  if (!isInitialized) {
    return nullptr;
  }

  int currentJob = job.fetch_add(1) % 64;
  if (currentJob == 0) {
    for (int i = 0; i < 64; ++i) {
      std::unique_lock<std::mutex> lock(mtx);
      jobs.push(i);
    }
    cv.notify_all();
  }
  while (!blocks_processed[currentJob])
    ;
  blocks_processed[currentJob] = false;
  return datas->at(currentJob).data();
}

#endif // ATF_PRANDOM_CPP