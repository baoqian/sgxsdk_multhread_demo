#include "../App.h"
#include "Enclave_u.h"
#include <thread>
#include <pthread.h>
#include <vector>

void thread_func(char *b, char *b_transposed, int m, int k, int n, int matrix_a_split, int num_thread, int thread_id) {
	sgx_status_t ret = SGX_ERROR_UNEXPECTED;
	ret = ecall_mul_thread(global_eid, b, b_transposed, m, k, n, matrix_a_split, num_thread, thread_id);
	if (ret != SGX_SUCCESS)
		abort();
}

void mul_thread(char *b, char *b_transposed, int m, int k, int n, int matrix_a_split, int num_thread) {
	printf("ecall_mul_thread\n");
	std::vector<std::thread *> threads(num_thread);
	for (int i = 0; i < num_thread; ++i) {
		threads[i] = new std::thread(thread_func, b, b_transposed, m, k, n, matrix_a_split, num_thread, i);
	}

	for (int i = 0; i < num_thread; ++i) {
		if (threads[i]->joinable()) {
			threads[i]->join();
		}
	}

	for (int i = 0; i < num_thread; ++i) {
		delete threads[i];
	}
	printf("\n");
}

void stdthread_func(std::unique_ptr<matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE>> &a, int num_thread, int thread_id) {
	sgx_status_t ret = SGX_ERROR_UNEXPECTED;
	ret = ecall_mul_stdthread(global_eid, (void *)(a.get()), num_thread, thread_id);
	if (ret != SGX_SUCCESS)
		abort();
}

void mul_stdthread(std::unique_ptr<matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE>> &a, int num_thread) {
	printf("mul_stdthread\n");
	std::vector<std::thread *> threads(num_thread);
	for (int i = 0; i < num_thread; ++i) {
		threads[i] = new std::thread(stdthread_func, std::ref(a), num_thread, i);
	}

	for (int i = 0; i < num_thread; ++i) {
		if (threads[i]->joinable()) {
			threads[i]->join();
		}
	}

	for (int i = 0; i < num_thread; ++i) {
		delete threads[i];
	}
	printf("\n");
}

static size_t global_res = 0;

void add(int i)
{
	size_t res = 0;
	sgx_status_t ret = SGX_ERROR_UNEXPECTED;
	ret = ecall_add(global_eid, &res, i);
	if (ret != SGX_SUCCESS)
		abort();
	if (res != 0) {
		global_res = res; 
	}
}

void test_mutex(void)
{
	std::thread t1(add, 0);
	std::thread t2(add, 1);
	std::thread t3(add, 2);
	std::thread t4(add, 3);

	t1.join();
	t2.join();
	t3.join();
	t4.join();

	printf("global_res = %d\n", global_res); // 200000
}

void data_producer()
{
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
    ret = ecall_producer(global_eid);
    if (ret != SGX_SUCCESS)
        abort();
}

void data_consumer()
{
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
    ret = ecall_consumer(global_eid);
    if (ret != SGX_SUCCESS)
        abort();
}

void data_producer_consumer() {
    std::thread consumer1(data_consumer);
    std::thread producer0(data_producer);
    std::thread consumer2(data_consumer);
    std::thread consumer3(data_consumer);
    std::thread consumer4(data_consumer);
    
    consumer1.join();
    consumer2.join();
    consumer3.join();
    consumer4.join();
    producer0.join();
}