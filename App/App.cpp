# define MAX_PATH FILENAME_MAX

#include "sgx_urts.h"
#include "App.h"
#include "Enclave_u.h"
#include <memory>
 #include <unistd.h>
#include <chrono>

/* Global EID shared by multiple threads */
sgx_enclave_id_t global_eid = 0;

typedef struct _sgx_errlist_t {
	sgx_status_t err;
	const char *msg;
	const char *sug; /* Suggestion */
} sgx_errlist_t;

/* Error code returned by sgx_create_enclave */
static sgx_errlist_t sgx_errlist[] = {
	{
		SGX_ERROR_UNEXPECTED,
		"Unexpected error occurred.",
		NULL
	},
	{
		SGX_ERROR_INVALID_PARAMETER,
		"Invalid parameter.",
		NULL
	},
	{
		SGX_ERROR_OUT_OF_MEMORY,
		"Out of memory.",
		NULL
	},
	{
		SGX_ERROR_ENCLAVE_LOST,
		"Power transition occurred.",
		"Please refer to the sample \"PowerTransition\" for details."
	},
	{
		SGX_ERROR_INVALID_ENCLAVE,
		"Invalid enclave image.",
		NULL
	},
	{
		SGX_ERROR_INVALID_ENCLAVE_ID,
		"Invalid enclave identification.",
		NULL
	},
	{
		SGX_ERROR_INVALID_SIGNATURE,
		"Invalid enclave signature.",
		NULL
	},
	{
		SGX_ERROR_OUT_OF_EPC,
		"Out of EPC memory.",
		NULL
	},
	{
		SGX_ERROR_NO_DEVICE,
		"Invalid SGX device.",
		"Please make sure SGX module is enabled in the BIOS, and install SGX driver afterwards."
	},
	{
		SGX_ERROR_MEMORY_MAP_CONFLICT,
		"Memory map conflicted.",
		NULL
	},
	{
		SGX_ERROR_INVALID_METADATA,
		"Invalid enclave metadata.",
		NULL
	},
	{
		SGX_ERROR_DEVICE_BUSY,
		"SGX device was busy.",
		NULL
	},
	{
		SGX_ERROR_INVALID_VERSION,
		"Enclave version was invalid.",
		NULL
	},
	{
		SGX_ERROR_INVALID_ATTRIBUTE,
		"Enclave was not authorized.",
		NULL
	},
	{
		SGX_ERROR_ENCLAVE_FILE_ACCESS,
		"Can't open enclave file.",
		NULL
	},
	{
		SGX_ERROR_NDEBUG_ENCLAVE,
		"The enclave is signed as product enclave, and can not be created as debuggable enclave.",
		NULL
	},
	{
		SGX_ERROR_MEMORY_MAP_FAILURE,
		"Failed to reserve memory for the enclave.",
		NULL
	},
};

/* Check error conditions for loading enclave */
void print_error_message(sgx_status_t ret)
{
	size_t idx = 0;
	size_t ttl = sizeof sgx_errlist/sizeof sgx_errlist[0];

	for (idx = 0; idx < ttl; idx++) {
		if(ret == sgx_errlist[idx].err) {
			if(NULL != sgx_errlist[idx].sug)
				printf("Info: %s\n", sgx_errlist[idx].sug);
			printf("Error: %s\n", sgx_errlist[idx].msg);
			break;
		}
	}
	
	if (idx == ttl)
		printf("Error: Unexpected error occurred.\n");
}

/* Initialize the enclave:
 *   Call sgx_create_enclave to initialize an enclave instance
 */
int initialize_enclave(void)
{
	sgx_status_t ret = SGX_ERROR_UNEXPECTED;
	
	/* Call sgx_create_enclave to initialize an enclave instance */
	/* Debug Support: set 2nd parameter to 1 */
	ret = sgx_create_enclave(ENCLAVE_FILENAME, SGX_DEBUG_FLAG, NULL, NULL, &global_eid, NULL);
	if (ret != SGX_SUCCESS) {
		print_error_message(ret);
		return -1;
	}

	return 0;
}

/* OCall functions */
void ocall_print_string(const char *str)
{
	/* Proxy/Bridge will check the length and null-terminate 
	 * the input string to prevent buffer overflow. 
	 */
	printf("%s", str);
}

std::chrono::time_point<std::chrono::high_resolution_clock> t1;
double timess = 400;
void ocall_get_time_start() {
	t1 = std::chrono::high_resolution_clock::now();
}

void ocall_get_time_end() {
	auto t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double, std::milli> time_span = t2 - t1;
	double times = time_span.count();
	// if (times < timess) {
	//	 timess = times;
	//	 fprintf(stderr, "test took %f ms\n", timess);
	// }
	fprintf(stderr, "test took %f ms\n", times);
}

void ocall_usleep() {
	usleep(1000000);
}

/* Application entry */
int SGX_CDECL main(int argc, char *argv[])
{
	(void)(argc);
	(void)(argv);


	/* Initialize the enclave */
	if(initialize_enclave() < 0){
		printf("Enter a character before exit ...\n");
		getchar();
		return -1;
	}

	int m, k, n;
	int matrix_a_split;
	int num_thread = 1;
	if (argc > 5) {
		m = atoi(argv[1]);
		k = atoi(argv[2]);
		n = atoi(argv[3]);
		matrix_a_split = atoi(argv[4]);
		num_thread = atoi(argv[5]);
	} else {
		m = 1;
		k = 4096;
		n = 11008;
		matrix_a_split = 4;
		num_thread = 12;
	}

	char *b = new char[k * n];
	for (int i = 0; i < k; ++i) {
		for (int j = 0; j < n; ++j) {
			b[i * n + j] = (char)(i + j);
		}
	}

	char *b_transposed = new char[n * k];
	int idx = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < k; ++j) {
			b_transposed[idx++] = b[j * n + i];
		}
	}

	std::unique_ptr<matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE>> a(new matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE>());

	// ecall_init_data(global_eid, m, k, n);
	// mul_thread(b, b_transposed, m, k, n, matrix_a_split, num_thread);
	// ecall_check_data(global_eid, m, k, n, b);
	for (int i = 0; i < matrix::A_ROW; ++i) {
		for (int j = 0; j < matrix::A_COL; ++j) {
			a->At(i, j) = (char)(i + j);
		}
	}

	data_producer_consumer();

	// ecall_init(global_eid, (void *)(a.get()));
	// mul_stdthread(a, num_thread);
	// ecall_check(global_eid);

	// ecall_mul_pthread(global_eid, (void*)(a.get()), num_thread);

	delete []b;
	delete []b_transposed;

	// test_mutex();
	ecall_test_prandom(global_eid);
	/* Destroy the enclave */
	sgx_destroy_enclave(global_eid);
	
	printf("Info: successfully returned.\n");

	return 0;
}

// ./app m k n