#ifndef _APP_H_
#define _APP_H_

#include <stdio.h>
#include <memory>
#include "sgx_error.h"	   /* sgx_status_t */
#include "sgx_eid.h"	 /* sgx_enclave_id_t */
#include "../Enclave/StaticMatrix.h"
#include "../Enclave/MatrixConfig.h"

#ifndef TRUE
# define TRUE 1
#endif

#ifndef FALSE
# define FALSE 0
#endif

#if   defined(__GNUC__)
# define ENCLAVE_FILENAME "enclave.signed.so"
#endif

extern sgx_enclave_id_t global_eid;	/* global enclave id */

#if defined(__cplusplus)
extern "C" {
#endif

void mul_thread(char *b, char *b_transposed, int m, int k, int n, int split, int row_split);
void mul_stdthread(std::unique_ptr<matrix::StaticMatrix<matrix::A_ROW, matrix::A_COL, matrix::A_TYPE>> &a, int num_thread);
void test_mutex();
void data_producer();
void data_consumer();
void data_producer_consumer();

#if defined(__cplusplus)
}
#endif

#endif /* !_APP_H_ */